package com.testapp.part1;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CalculatorTest {

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testGetSum() {

        int a=2;
        int b=3;
        int actual=a+b;
        int expected=5;
        assertEquals(actual,expected);
    }

    @Test
    public void testGetDifference() {

        int a=4;
        int b=1;
        int actual=a-b;
        int expected=3;
        assertEquals(actual,expected);
    }

    @Test
    public void testGetMultiplication() {
        int a=5;
        int b=4;
        int actual=a*b;
        int expected=20;
        assertEquals(actual,expected);
    }

    @Test
    public void testGetQuotient() {
        int a=12;
        int b=3;
        int actual=a/b;
        int expected=4;
        assertEquals(actual,expected);
    }
}