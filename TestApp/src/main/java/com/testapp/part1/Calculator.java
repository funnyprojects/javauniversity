package com.testapp.part1;

public class Calculator {

    public int getSum(int a,int b){
        return a+b;
    }

    public int getDifference(int a,int b){
        return a-b;
    }

    public int getMultiplication(int a,int b){
        return a*b;
    }

    public double getQuotient(double a,double b){

        if (b==0){
            return 0;
        }
        return a/b;
    }




}
