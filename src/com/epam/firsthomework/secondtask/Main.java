package com.epam.firsthomework.secondtask;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int algorithmId;
        do {
            algorithmId = enterFromConsole("Choose action: 1-calculating a series of Fibonacci numbers, 2-calculating a factorial");
        } while (algorithmId != 1 && algorithmId != 2);

        int loopType;
        do {
            loopType = enterFromConsole("Choose loop type: 1-while, 2-do-while, 3-for");
        } while (loopType != 1 && loopType != 2 && loopType != 3);

        int num;
        do {
            num = enterFromConsole("Input positive num");
        } while (num <= 0);

        if (algorithmId == 1) {
            int[] fibonacciNumbers = calculateFibonacciNumbers(num, loopType);

            System.out.println(String.format("%d Fibonacci numbers", num));
            for (int i = 0; i < fibonacciNumbers.length; i++) {
                System.out.print(fibonacciNumbers[i] + " ");
            }
            System.out.println();

        } else {
            int factorial = getFactorial(num, loopType);
            System.out.println(String.format("Factorial of %d = %d", num, factorial));
        }

    }

    public static int enterFromConsole(String message) {

        Scanner sc = new Scanner(System.in);

        System.out.println(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.println(message);
        }
        int num = sc.nextInt();

        return num;

    }

    public static int[] calculateFibonacciNumbers(int num, int loopType) {

        int[] fibonacciNumbers = new int[num];

        if (num == 1) {
            fibonacciNumbers[0] = 0;
        } else {
            fibonacciNumbers[0] = 0;
            fibonacciNumbers[1] = 1;
            int currentIndex = 2;

            if (fibonacciNumbers.length > currentIndex) {
                switch (loopType) {
                    case 1:
                        while (currentIndex < num) {
                            fibonacciNumbers[currentIndex] = fibonacciNumbers[currentIndex - 1] + fibonacciNumbers[currentIndex - 2];
                            currentIndex++;
                        }
                        break;
                    case 2:
                        do {
                            fibonacciNumbers[currentIndex] = fibonacciNumbers[currentIndex - 1] + fibonacciNumbers[currentIndex - 2];
                            currentIndex++;
                        } while (currentIndex < num);
                        break;
                    case 3:
                        for (int i = currentIndex; i < fibonacciNumbers.length; i++) {
                            fibonacciNumbers[i] = fibonacciNumbers[i - 1] + fibonacciNumbers[i - 2];
                        }
                        break;
                }
            }


        }

        return fibonacciNumbers;
    }

    public static int getFactorial(int num, int loopType) {
        int factorial = 1;
        int currentIndex = 1;

        switch (loopType) {
            case 1:
                while (currentIndex <= num) {
                    factorial *= currentIndex;
                    currentIndex++;
                }
                break;
            case 2:
                do {
                    factorial *= currentIndex;
                    currentIndex++;
                } while (currentIndex <= num);
            case 3:
                for (int i = currentIndex; i <= num; i++) {
                    factorial *= currentIndex;
                }
        }

        return factorial;
    }

}
