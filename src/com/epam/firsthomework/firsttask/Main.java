package com.epam.firsthomework.firsttask;

public class Main {

    public static void main(String[] args){

        if (args.length != 4) {
            System.out.println("Wrong number of arguments. It must be 4");
        } else if (Double.parseDouble(args[2]) + Double.parseDouble(args[3]) == 0 || Integer.parseInt(args[1]) == 0) {
            System.out.println("Dividing by zero is not correct");
        } else {
            double g;

            g = 4 * Math.pow(Math.PI, 2);
            g *= Math.pow(Integer.parseInt(args[0]), 3);
            g /= Math.pow(Integer.parseInt(args[1]), 2);
            g /= Double.parseDouble(args[2]) + Double.parseDouble(args[3]);

            System.out.println(String.format("G = %f", g));
        }
        
    }

}
