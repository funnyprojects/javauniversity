package com.epam.collections.main;

import com.epam.collections.factories.BrigadeFactory;
import com.epam.collections.factories.WorkerFactory;
import com.epam.collections.models.Brigade;
import com.epam.collections.models.RequirementManager;
import com.epam.collections.services.DecisionMaker;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        BrigadeFactory brigadeFactory = new BrigadeFactory();
        Random rnd = new Random();

        int brigadeCount = rnd.nextInt(10) + 1;
        List<Brigade> brigadeList = new ArrayList();
        for (int i = 0; i < brigadeCount; i++) {
            brigadeList.add(brigadeFactory.createBrigade(i + 1, rnd));
            System.out.println(brigadeList.get(i).getInfo());
        }

        RequirementManager requirementManager = new RequirementManager(rnd);
        System.out.println("Requirements");
        System.out.println(requirementManager.getInfo());

        DecisionMaker decisionMaker = new DecisionMaker(brigadeList, requirementManager.getRequirements());
        Optional<Brigade> chosenBrigade = decisionMaker.getDecision();
        System.out.println("Chosen Brigade:");
        System.out.println(chosenBrigade.isPresent() ? chosenBrigade.get().getInfo() : "There is no decision");

    }
}
