package com.epam.collections.services;

import com.epam.collections.models.Brigade;
import com.epam.collections.models.Skills;

import java.util.*;
import java.util.stream.Stream;

public class DecisionMaker {

    private List<Brigade> brigadeList;
    private Map<Skills, Integer> requirementsMap;

    public DecisionMaker(List<Brigade> brigadeList, Map<Skills, Integer> requirementsMap) {
        this.brigadeList = brigadeList;
        this.requirementsMap = requirementsMap;
    }

    public Optional<Brigade> getDecision() {
        return brigadeList.stream()
                .filter(brigade -> Stream.of(Skills.values())
                        .filter(skill -> brigade.calculateSkillsMap().get(skill) < requirementsMap.get(skill)).count() == 0)
                .sorted()
                .findFirst();
    }
}