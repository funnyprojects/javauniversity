package com.epam.collections.models;

public enum Skills {
    INTERNALWORK,
    EXTERNALWORK,
    ELECTRICALWIRING,
    MATERIALPROPERTIES,
    READINGDRAWINGS,
    RECONSTRUCTIONWORK
}
