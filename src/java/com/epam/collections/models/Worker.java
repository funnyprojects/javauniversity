package com.epam.collections.models;

import com.epam.collections.models.Skills;

import java.util.Set;


public class Worker {

    private Set<Skills> skills;
    private int salary;

    public Worker(Set<Skills> skills, int salary) {
        this.skills = skills;
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public Set<Skills> getSkills() {
        return skills;
    }

    public String getInfo() {
        return String.format("Skills: %s, Salary: %s", skills, salary);
    }

}
