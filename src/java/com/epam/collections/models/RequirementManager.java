package com.epam.collections.models;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class RequirementManager {

    private Map<Skills, Integer> requirementsMap;

    public RequirementManager(Random rnd) {
        requirementsMap = new HashMap<Skills, Integer>();
        initRequirements(rnd);
    }

    private void initRequirements(Random rnd) {
        for (int i = 0; i < Skills.values().length; i++) {
            requirementsMap.put(Skills.values()[i], rnd.nextInt(2) + 1);
        }
    }

    public String getInfo() {
        String info = "";
        for (Map.Entry<Skills, Integer> item : requirementsMap.entrySet()) {
            info += item.getKey() + ":" + item.getValue() + "\n";
        }
        return info;
    }


    public Map<Skills, Integer> getRequirements() {
        return requirementsMap;
    }

}
