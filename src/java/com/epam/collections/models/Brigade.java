package com.epam.collections.models;

import java.util.*;
import java.util.stream.Stream;

public class Brigade implements Comparable<Brigade> {

    private int id;
    private List<Worker> workersList;
    private Map<Skills, Integer> skillsMap;

    public Brigade(int id, List<Worker> workersList) {
        this.id = id;
        this.workersList = workersList;
        skillsMap = new HashMap<Skills, Integer>();
    }

    public int getId() {
        return id;
    }

    public Map<Skills, Integer> calculateSkillsMap() {

        Stream.of(Skills.values()).forEach(skill -> skillsMap.put(skill, (int) workersList.stream()
                .filter(worker -> worker.getSkills().contains(skill)).count()));
        return skillsMap;
    }

    public int getSalariesSum() {
        return workersList.stream().map(worker -> worker.getSalary()).reduce(0, Integer::sum);
    }

    public String getInfo() {
        String info = String.format("Brigade %d\n", id);
        for (int i = 0; i < workersList.size(); i++) {
            info += i + 1 + " Worker: " + workersList.get(i).getInfo() + '\n';
        }

        info += "Salaries sum :" + getSalariesSum();
        return info;
    }

    public int compareTo(Brigade brigade) {
        if (this.getSalariesSum() > brigade.getSalariesSum()) {
            return 1;
        } else if (this.getSalariesSum() < brigade.getSalariesSum()) {
            return -1;
        }
        return 0;
    }


}
