package com.epam.collections.factories;

import com.epam.collections.models.Brigade;
import com.epam.collections.models.Worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BrigadeFactory {
    private List<Worker> workersList;
    private WorkerFactory workerFactory;

    public BrigadeFactory() {
        workerFactory = new WorkerFactory();
    }

    public Brigade createBrigade(int id, Random rnd) {
        workersList = new ArrayList<Worker>();
        int workersCount = rnd.nextInt(5) + 5;

        for (int i = 0; i < workersCount; i++) {
            workersList.add(workerFactory.createWorker(rnd));
        }
        return new Brigade(id, workersList);
    }

}
