package com.epam.collections.factories;

import com.epam.collections.models.Skills;
import com.epam.collections.models.Worker;

import java.util.*;

public class WorkerFactory {

    private int salary;
    private HashSet<Skills> skills;

    public Worker createWorker(Random rnd) {
        salary = rnd.nextInt(10000) + 35000;
        skills = new HashSet();
        for (int i = 0; i < rnd.nextInt(5) + 1; i++) {
            skills.add(Skills.values()[rnd.nextInt(6)]);
        }

        return new Worker(skills, salary);
    }
}
